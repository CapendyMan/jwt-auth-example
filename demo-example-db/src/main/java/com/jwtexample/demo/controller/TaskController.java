package com.jwtexample.demo.controller;

import com.jwtexample.demo.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    @Autowired
    TaskRepository taskRepository;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all")
    public ResponseEntity getTasks() {
        return new ResponseEntity(taskRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{title}")
    public ResponseEntity getTask(@PathVariable("title") final String title) {
        return new ResponseEntity(taskRepository.getTaskByTitle(title), HttpStatus.OK);
    }
}
