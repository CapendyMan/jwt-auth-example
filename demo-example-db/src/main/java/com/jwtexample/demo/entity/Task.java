package com.jwtexample.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Setter
@Getter
@Entity
public class Task implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    private String title;
    private String description;
}
