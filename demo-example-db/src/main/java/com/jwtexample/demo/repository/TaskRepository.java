package com.jwtexample.demo.repository;

import com.jwtexample.demo.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long>{
    Task getTaskByTitle(String title);
    List<Task> findAll();
}
