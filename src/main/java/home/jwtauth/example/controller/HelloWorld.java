package home.jwtauth.example.controller;

import home.jwtauth.example.service.HelloWorldService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/hello")
public class HelloWorld {

    @Autowired
    HelloWorldService helloWorldService;

    @GetMapping
    public String hello (@RequestParam("name") final String name) {
        return helloWorldService.hello(name);
    }
}
