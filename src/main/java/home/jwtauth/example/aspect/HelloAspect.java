package home.jwtauth.example.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class HelloAspect {

    @Before(value = "execution(* home.jwtauth.example.service.HelloWorldService.*(..)) && args(name)")
    public void beforeAdvice(JoinPoint joinPoint, String name) {

        System.out.println("Before method:" + joinPoint.getSignature());

        System.out.println("Hello, " + name);
    }

    @After(value = "execution(* home.jwtauth.example.service.HelloWorldService.*(..)) && args(name)")
    public void afterAdvice(JoinPoint joinPoint, String name) {
        System.out.println("After method:" + joinPoint.getSignature());

        System.out.println("By, " + name);
    }
}
